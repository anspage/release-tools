# frozen_string_literal: true

require_relative 'lib/release_tools'
require_relative 'lib/release_tools/tasks'

# TODO: helpers for backward compatibility
include ReleaseTools::Tasks::Helper

# monkeypatching to ensure uncaught exception are logged on elastic search
module Rake
  class Task
    include ::SemanticLogger::Loggable

    alias_method :invoke_without_log, :invoke

    def invoke(*args)
      begin
        invoke_without_log(*args)
      ensure
        last_exception = $!
        return if last_exception.nil?

        logger.fatal('Task failed', last_exception)
      end
    end
  end
end

Dir.glob('lib/tasks/*.rake').each { |task| import(task) }

# Undocumented; executed via CI schedule
task :close_expired_qa_issues do
  ReleaseTools::Qa::IssueCloser.new.execute
end

desc "Publish packages, tags, stable branches, CNG images, and Helm chart for a specified version"
task :publish, [:version] do |_t, args|
  version = get_version(args)

  ReleaseTools::Services::OmnibusPublishService
    .new(version)
    .execute

  # Ensure any exceptions raised by this new service don't fail the build, since
  # all destructive behaviors are behind feature flags.
  Raven.capture do
    ReleaseTools::Services::SyncRemotesService
      .new(version)
      .execute
  end

  # Both the CNG and Helm charts publish jobs rely on the tag having been pushed
  # to gitlab.com, so we run these after the remote sync
  ReleaseTools::Services::CNGPublishService
    .new(version)
    .execute

  chart_version = ReleaseTools::Helm::HelmVersionFinder.new.execute(version)
  ReleaseTools::Services::HelmChartPublishService
    .new(chart_version)
    .execute
end
