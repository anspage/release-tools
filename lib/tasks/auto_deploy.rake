# frozen_string_literal: true

namespace :auto_deploy do
  desc "Prepare for auto-deploy by creating branches from the latest green commit on gitlab and omnibus-gitlab"
  task :prepare do
    ReleaseTools::Tasks::AutoDeploy::Prepare.new.execute
  end

  desc 'Pick commits into the auto deploy branches'
  task :pick do
    pick = lambda do |project, branch_name|
      ReleaseTools.logger.info(
        'Picking into auto-deploy branch',
        project: project.auto_deploy_path,
        target: branch_name
      )

      ReleaseTools::CherryPick::AutoDeployService
        .new(project, branch_name)
        .execute
    end

    branch_name = ReleaseTools::AutoDeployBranch.current_name

    pick[ReleaseTools::Project::GitlabEe, branch_name]
    pick[ReleaseTools::Project::OmnibusGitlab, branch_name]
  end

  desc "Tag the auto-deploy branches from the latest passing builds"
  task :tag do
    ReleaseTools::Tasks::AutoDeploy::Tag.new.execute
  end

  desc "Validate production pre-checks"
  task :check_production do
    ReleaseTools::Tasks::AutoDeploy::CheckProduction.new.execute
  end

  desc 'Cleans up old auto-deploy branches'
  task :cleanup do
    ReleaseTools::AutoDeploy::Cleanup.new.cleanup
  end
end
