# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    class Tag
      include Singleton

      # We purposefully want this to be defined at runtime
      NOW = Time.now.utc.freeze

      # Return the current auto-deploy tag string
      #
      # If running on a tagged release-tools pipeline, this will simply return
      # that tag.
      #
      # Otherwise, we build the tag string from the current auto-deploy branch
      # and the current UTC timestamp.
      def self.current
        ENV.fetch('CI_COMMIT_TAG') { instance.to_s }
      end

      def initialize
        @branch = ReleaseTools::AutoDeployBranch.current
      end

      def to_s
        "#{@branch.version.to_minor}.#{timestamp}"
      end

      def timestamp
        NOW.strftime('%Y%m%d%H%M')
      end
    end
  end
end
