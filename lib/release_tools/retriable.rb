# frozen_string_literal: true

require 'retriable'

Retriable.configure do |config|
  config.contexts[:api] = {
    on: [
      ::Gitlab::Error::ResponseError,
      Timeout::Error,
      Errno::ECONNRESET
    ]
  }
end
