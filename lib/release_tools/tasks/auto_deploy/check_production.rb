# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      # CheckProduction verifies production status.
      #
      # It can authorize a deployment if DEPLOY_VERSION variable is set.
      # It can notify production status on slack if CHAT_CHANNEL is set.
      class CheckProduction
        include ::SemanticLogger::Loggable

        def execute
          manager = ReleaseTools::Promotion::Manager.new(ENV['DEPLOYER_PIPELINE_URL'])

          if chatops?
            logger.info("Checking production status", slack_channel: slack_channel)

            manager.check_status(slack_channel)
          elsif authorize?
            logger.info("Checking production promotion", deploy_version: deploy_version)

            manager.authorize!(omnibus_package_version, monthly_issue)
          elsif baking_time?
            logger.info("Checking production status after baking time", deploy_version: deploy_version)

            manager.baking_time_report(omnibus_package_version)
          elsif deployment_check?
            return unless Feature.enabled?(:deployment_check)

            logger.info("Checking production deployment status", deploy_version: deploy_version, deployment_step: deployment_step, deployment_job_url: deployment_job_url)

            manager.deployment_check_report(omnibus_package_version, deployment_step, deployment_job_url)
          else
            raise 'This task requires one of the following environment variables. DEPLOY_VERSION: containing the package version to authorize, or CHAT_CHANNEL: the slack channel id for writing the check results'
          end
        end

        def monthly_issue
          ReleaseTools::MonthlyIssue.new(version: monthly_version)
        end

        def monthly_version
          ReleaseManagers::Schedule.new.active_version
        end

        def omnibus_package_version
          ReleaseTools::Version.new(deploy_version)
        end

        def slack_channel
          ENV['CHAT_CHANNEL']
        end

        def deploy_version
          ENV['DEPLOY_VERSION']
        end

        def chatops?
          slack_channel.present?
        end

        def authorize?
          deploy_version.present? && !baking_time? && !deployment_check?
        end

        def baking_time?
          ENV['BAKING_TIME'] == 'true'
        end

        def deployment_check?
          ENV['DEPLOYMENT_CHECK'] == 'true'
        end

        def deployment_step
          ENV['DEPLOYMENT_STEP']
        end

        def deployment_job_url
          ENV['DEPLOYER_JOB_URL']
        end
      end
    end
  end
end
