# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class Tag
        include ::SemanticLogger::Loggable

        attr_reader :branch, :metadata

        def initialize
          @branch = ReleaseTools::AutoDeployBranch.current
          @metadata = ReleaseTools::ReleaseMetadata.new
        end

        def execute
          find_passing_build

          omnibus_changes = build_omnibus
          cng_changes = build_cng

          tag_coordinator if omnibus_changes || cng_changes

          sentry_release
          upload_metadata
        end

        def find_passing_build
          logger.info('Searching for commit with passing build', branch: branch.to_s)
          commit
        end

        def build_omnibus
          logger.info('Building Omnibus', commit: commit.id)
          ReleaseTools::AutoDeploy::Builder::Omnibus
            .new(branch, commit.id, metadata)
            .execute
        end

        def build_cng
          logger.info('Building CNG', commit: commit.id)
          ReleaseTools::AutoDeploy::Builder::CNGImage
            .new(branch, commit.id, metadata)
            .execute
        end

        def tag_coordinator
          ReleaseTools::AutoDeploy::Tagger::Coordinator.new.tag!
        end

        def sentry_release
          logger.info('Creating Sentry release', commit: commit.id)
          ReleaseTools::Deployments::SentryTracker.new.release(commit.id)
        end

        def upload_metadata
          version = ReleaseTools::AutoDeploy::Tag.current

          logger.info('Uploading metadata', version: version)
          ReleaseTools::ReleaseMetadataUploader.new.upload(version, metadata)
        end

        private

        def commit
          @commit ||= ReleaseTools::PassingBuild.new(branch.to_s).execute
        end
      end
    end
  end
end
