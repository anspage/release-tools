# frozen_string_literal: true

module ReleaseTools
  module Promotion
    module Checks
      # ActiveIncidents checks for active incidents issue with at least severity::3
      class ActiveIncidents
        include ProductionIssueTracker

        def name
          'active incidents'
        end

        def labels
          'Incident::Active'
        end

        def filter(issue)
          issue.labels.include?('severity::1') ||
            issue.labels.include?('severity::2') ||
            issue.labels.include?('severity::3')
        end
      end
    end
  end
end
