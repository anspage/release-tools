# frozen_string_literal: true

module ReleaseTools
  # Represents an auto-deploy branch for purposes of cherry-picking
  class AutoDeployBranch
    attr_reader :version, :branch_name

    # Return the current auto-deploy branch name from environment variable
    def self.current_name
      ENV.fetch('AUTO_DEPLOY_BRANCH')
    end

    def self.current
      new(current_name)
    end

    def initialize(name)
      major, minor = name.split('-', 3).take(2)

      @version = Version.new("#{major}.#{minor}")
      @branch_name = name
    end

    def to_s
      branch_name
    end
  end
end
