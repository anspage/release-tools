# frozen_string_literal: true

require 'prometheus/client'
require 'prometheus/client/push'

require 'release_tools/metrics/push'

require 'release_tools/metrics/auto_deploy_pressure'

module ReleaseTools
  module Metrics
  end
end
