stages:
  - test
  - deploy
  - automation
  - chatops
  - metrics

default:
  image: "dev.gitlab.org:5005/gitlab/gitlab-build-images:release-tools"
  cache:
    key:
      files:
        - .ruby-version
    paths:
      - vendor/ruby
  tags:
    - gitlab-org

# templates -------------------------------------------------------------------

.except-automation: &except-automation
  except:
    - schedules
    - triggers
    - pipelines
    - tags

.except-automation-rules: &except-automation-rules
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "pipeline"
      when: never
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: on_success

.with-bundle: &with-bundle
  before_script:
    - bundle install --jobs=$(nproc) --retry=3 --path=vendor --quiet

# test ------------------------------------------------------------------------

rubocop:
  <<: *except-automation
  <<: *with-bundle
  stage: test
  script:
    - bundle exec rubocop

specs:
  <<: *except-automation
  <<: *with-bundle
  stage: test
  script:
    - git config --global user.email "you@example.com"
    - git config --global user.name "Your Name"
    - bundle exec rspec
  artifacts:
    paths:
      - coverage/assets
      - coverage/index.html

# deploy ----------------------------------------------------------------------

pages:
  <<: *except-automation
  stage: deploy
  script:
    - mkdir -p public/
    - mv coverage/ public/
  dependencies:
    - specs
  artifacts:
    paths:
      - public/
  only:
    - master

# automation ------------------------------------------------------------------

release-managers:
  <<: *with-bundle
  stage: automation
  script:
    - source scripts/setup_ssh.sh
    - bundle exec rake release_managers:sync
  only:
    refs:
      - schedules
    variables:
      - $RELEASE_MANAGERS
  cache:
    policy: pull

close-expired-qa-issues:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake close_expired_qa_issues
  only:
    refs:
      - schedules
    variables:
      - $CLOSE_EXPIRED_QA_ISSUES

validate-security-merge-requests:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake security:validate
  only:
    refs:
      - schedules
    variables:
      - $VALIDATE_SECURITY_MERGE_REQUESTS

security:merge-train:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake 'security:merge_train'
  rules:
    - if: $SECURITY_MERGE_TRAIN == '1'
      when: always

auto_deploy:prepare:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake 'auto_deploy:prepare'
  only:
    refs:
      - schedules
    variables:
      - $CREATE_AUTO_DEPLOY_BRANCH_SCHEDULE == "true"

auto_deploy:pick:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake 'auto_deploy:pick'
  only:
    variables:
      - $CHERRY_PICK_AUTO_DEPLOY_BRANCH_SCHEDULE == "true"

auto_deploy:tag:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake 'auto_deploy:tag'
  only:
    variables:
      - $PASSING_BUILD_AUTO_DEPLOY_SCHEDULE == "true"

auto_deploy:tag:coordinated:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake 'auto_deploy:tag'
  only:
    refs:
      - tags

auto_deploy:cleanup:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake 'auto_deploy:cleanup'
  rules:
    - if: $AUTO_DEPLOY_CLEANUP == "true"
      when: always

components:update_gitaly:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake 'components:update_gitaly'
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" ||  $CI_PIPELINE_SOURCE == "push"
      when: never    # don't run on code changes, either merge requests on branch pipelines
    - if: $UPDATE_COMPONENTS == "true"

track-deployment:
  <<: *with-bundle
  stage: automation
  script:
    - bundle exec rake "release:track_deployment[$DEPLOY_ENVIRONMENT, $DEPLOY_STATUS, $DEPLOY_VERSION]"
  artifacts:
    expire_in: 1d
    paths:
      - QA_ISSUE_URL
  only:
    variables:
      - $TRACK_DEPLOYMENT == "true"

release:tag_scheduled_rc:
  <<: *with-bundle
  stage: automation
  script:
    - source scripts/setup_ssh.sh
    - source scripts/setup_git.sh
    - bundle exec rake 'release:tag_scheduled_rc'
  only:
    refs:
      - schedules
    variables:
      - $TAG_SCHEDULED_RC == "true"

auto_deploy:check_production:
  <<: *with-bundle
  stage: automation
  tags:
    # Internal prometheus is only available from specific tagged runners
    - release
  script:
    - bundle exec rake 'auto_deploy:check_production'
  rules:
    - if: $CHECK_PRODUCTION == "true"
  cache:
    key: 'deployment_notification_ts'
    paths:
      - SLACK_DEPLOYMENT_MESSAGE_TS
      - vendor/ruby

# chatops ---------------------------------------------------------------------

chatops:
  <<: *with-bundle
  stage: chatops
  script:
    - source scripts/setup_ssh.sh
    - source scripts/setup_git.sh
    - ./bin/chatops $TASK
  only:
    refs:
      - triggers
    variables:
      - $TASK
  cache:
    key:
      files:
        - .ruby-version
    paths:
      - vendor/ruby
    policy: pull

# metrics ---------------------------------------------------------------------

metrics:
  <<: *with-bundle
  stage: metrics
  rules:
    - if: '$PROMETHEUS_HOST && $PUSHGATEWAY_URL && $PUSH_METRICS'
      when: always
  tags:
    # Internal pushgateway is only available from specific tagged runners
    - release
  script:
    - bundle exec rake metrics
