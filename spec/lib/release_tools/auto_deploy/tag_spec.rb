# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::AutoDeploy::Tag do
  describe '.current' do
    context 'in a tagged pipeline' do
      it 'returns the current tag' do
        ClimateControl.modify(CI_COMMIT_TAG: 'some-tag') do
          expect(described_class.current).to eq('some-tag')
        end
      end
    end

    context 'in an untagged pipeline' do
      it 'builds the tag from the current auto-deploy branch' do
        env = {
          CI_COMMIT_TAG: nil,
          AUTO_DEPLOY_BRANCH: '1-2-auto-deploy'
        }

        ClimateControl.modify(env) do
          Timecop.freeze(Time.new(2015, 1, 2, 3, 4, 0, 'UTC')) do
            # Even though we're freezing time here, the `NOW` constant was
            # already defined as soon as we ran `rspec`.
            #
            # As a workaround, stub the constant to our new current time, which
            # is actually the past. Great Scott!
            stub_const("#{described_class}::NOW", Time.now.utc)
          end

          expect(described_class.current).to eq('1.2.201501020304')
        end
      end
    end
  end
end
