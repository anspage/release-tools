# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::ProductionStatus do
  subject(:status) { described_class.new }

  describe '#fine?' do
    context 'when all the checks are fine' do
      it 'is fine' do
        checks = [
          double('check 1', fine?: true),
          double('check 2', fine?: true)
        ]

        expect(status).to receive(:checks).and_return(checks)

        expect(status).to be_fine
      end
    end

    context 'when at least one check failed' do
      it 'is not fine' do
        checks = [
          double('check 1', fine?: false),
          double('check 2', fine?: true)
        ]

        expect(status).to receive(:checks).and_return(checks)

        expect(status).not_to be_fine
      end
    end
  end

  describe '#to_slack_blocks' do
    let(:fine_status) { true }

    let(:checks) do
      [
        double('check 1', to_slack_block: { text: 'result check 1' }),
        double('check 2', to_slack_block: { text: 'result check 2' })
      ]
    end

    before do
      allow(status).to receive(:fine?).and_return(fine_status)
      allow(status).to receive(:checks).and_return(checks)
    end

    context 'when deployment is about to start' do
      subject { status.to_slack_blocks.first.dig(:text, :text) }

      context 'when it can start' do
        it { is_expected.to eq(':white_check_mark: We can start a deployment now! :shipit: :fine:') }

        it 'shows check blocks' do
          slack_blocks = status.to_slack_blocks

          expect(slack_blocks.length).to eq(3)
          expect(slack_blocks[1][:text]).to eq('result check 1')
          expect(slack_blocks[2][:text]).to eq('result check 2')
        end
      end

      context 'when checks are not fine' do
        let(:fine_status) { false }

        it { is_expected.to eq(':red_circle: A deployment cannot start now!') }

        it 'shows check blocks' do
          slack_blocks = status.to_slack_blocks

          expect(slack_blocks.length).to eq(3)
          expect(slack_blocks[1][:text]).to eq('result check 1')
          expect(slack_blocks[2][:text]).to eq('result check 2')
        end
      end
    end

    context 'when there are blockers in a deployment in progress' do
      subject { status.to_slack_blocks(deployment_in_progress: true).first.dig(:text, :text) }

      let(:fine_status) { false }

      it { is_expected.to include(':red_circle: There are blockers!') }

      it 'shows check blocks' do
        slack_blocks = status.to_slack_blocks(deployment_in_progress: true)

        expect(slack_blocks.length).to eq(3)
        expect(slack_blocks[1][:text]).to eq('result check 1')
        expect(slack_blocks[2][:text]).to eq('result check 2')
      end
    end
  end
end
