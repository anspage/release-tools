# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::AutoDeploy::CheckProduction do
  let(:deploy_version) { '' }
  let(:chat_channel) { '' }
  let(:active_version) { ReleaseTools::Version.new('13.1') }
  let(:manager) { instance_double(ReleaseTools::Promotion::Manager) }

  let(:now) { Time.local(2020, 2, 22, 16) }

  subject(:task) { described_class.new }

  before do
    schedule =
      instance_double(ReleaseTools::ReleaseManagers::Schedule,
                      active_version: active_version)
    allow(ReleaseTools::ReleaseManagers::Schedule).to receive(:new).and_return(schedule)
    allow(ReleaseTools::Promotion::Manager).to receive(:new).and_return(manager)
  end

  around do |example|
    ClimateControl.modify(DEPLOY_VERSION: deploy_version, CHAT_CHANNEL: chat_channel, &example)
  end

  describe '#monthly_issue' do
    it 'returns the issue for the current milestone' do
      expect(ReleaseTools::MonthlyIssue).to receive(:new).with(version: active_version)

      task.monthly_issue
    end
  end

  context 'when triggered by the deployer' do
    let(:deploy_version) { '13.1.202005220540-7c84ccdc806.59f00bb0515' }

    it { is_expected.to be_authorize }
    it { is_expected.not_to be_baking_time }
    it { is_expected.not_to be_chatops }

    context 'when BAKING_TIME is true' do
      around do |example|
        ClimateControl.modify(BAKING_TIME: 'true', &example)
      end

      it { is_expected.to be_baking_time }
      it { is_expected.not_to be_authorize }
      it { is_expected.not_to be_chatops }

      describe '#execute' do
        it 'will not attempt to authorize a deployment' do
          expect(manager).to receive(:baking_time_report).with(
            ReleaseTools::Version.new(deploy_version)
          )

          expect(manager).not_to receive(:authorize!)
          expect(manager).not_to receive(:check_status)

          task.execute
        end
      end
    end

    context 'when DEPLOYMENT_CHECK is true' do
      let(:deployment_step) { 'gprd-cny-migrations' }
      let(:job_url) { 'https://test.net/deployer/-/jobs/123' }

      before do
        enable_feature(:deployment_check)
      end

      around do |example|
        ClimateControl.modify(
          BAKING_TIME: '',
          DEPLOYMENT_CHECK: 'true',
          DEPLOYMENT_STEP: deployment_step,
          DEPLOYER_JOB_URL: job_url,
          &example
        )
      end

      it { is_expected.to be_deployment_check }
      it { is_expected.not_to be_baking_time }
      it { is_expected.not_to be_authorize }
      it { is_expected.not_to be_chatops }

      it 'executes a deployment check' do
        expect(manager)
          .to receive(:deployment_check_report)
          .with(deploy_version, deployment_step, job_url)

        task.execute
      end

      context 'when feature flag is not enabled' do
        it 'skips deployment check' do
          disable_feature(:deployment_check)

          allow(manager).to receive(:authorize!)
            .and_return(false)

          expect(manager).not_to receive(:deployment_check_report)

          task.execute
        end
      end
    end

    describe '#deploy_version' do
      it 'matches the environment variable' do
        expect(task.deploy_version).to eq(deploy_version)
      end
    end

    describe '#execute' do
      it 'will attempt to authorize a deployment' do
        expect(manager).to receive(:authorize!).with(
          ReleaseTools::Version.new(deploy_version),
          ReleaseTools::MonthlyIssue
        )

        expect(manager).not_to receive(:check_status)
        expect(manager).not_to receive(:baking_time_report)

        task.execute
      end
    end
  end

  context 'when triggered by chatops' do
    let(:chat_channel) { 'UG018231341' }

    it { is_expected.to be_chatops }
    it { is_expected.not_to be_authorize }
    it { is_expected.not_to be_baking_time }

    describe '#chat_channel' do
      it 'matches the environment variable' do
        expect(task.slack_channel).to eq(chat_channel)
      end
    end

    describe '#execute' do
      it 'will publish the production status message on slack' do
        expect(manager).to receive(:check_status).with(chat_channel)
        expect(manager).not_to receive(:authorize!)
        expect(manager).not_to receive(:baking_time_report)

        task.execute
      end
    end
  end
end
