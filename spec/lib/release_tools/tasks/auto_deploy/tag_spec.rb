# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::AutoDeploy::Tag do
  subject(:task) { described_class.new }

  around do |ex|
    ClimateControl.modify(AUTO_DEPLOY_BRANCH: '42-1-auto-deploy-2021010200') do
      ex.run
    end
  end

  def stub_passing_build(commit_id)
    passing_build = instance_double(
      'PassingBuild',
      execute: double('Commit', id: commit_id)
    )

    allow(ReleaseTools::PassingBuild).to receive(:new)
      .and_return(passing_build)
  end

  describe '#execute' do
    it 'performs a full tagging run' do
      expect(task).to receive(:find_passing_build)
      expect(task).to receive(:build_omnibus).and_return(false)
      expect(task).to receive(:build_cng).and_return(true)
      expect(task).to receive(:sentry_release)
      expect(task).to receive(:upload_metadata)

      expect(task).to receive(:tag_coordinator)

      task.execute
    end

    it 'does not tag coordinator without builder changes' do
      expect(task).to receive(:find_passing_build)
      expect(task).to receive(:build_omnibus).and_return(false)
      expect(task).to receive(:build_cng).and_return(false)
      expect(task).to receive(:sentry_release)
      expect(task).to receive(:upload_metadata)

      expect(task).not_to receive(:tag_coordinator)

      task.execute
    end
  end

  describe '#find_passing_build' do
    it 'finds a passing build' do
      passing_build = stub_const('ReleaseTools::PassingBuild', spy)

      task.find_passing_build

      expect(passing_build).to have_received(:new).with(task.branch.to_s)
      expect(passing_build).to have_received(:execute)
    end
  end

  describe '#build_omnibus' do
    it 'executes Omnibus builder' do
      stub_passing_build('b81056529d1f')
      builder = stub_const('ReleaseTools::AutoDeploy::Builder::Omnibus', spy)

      task.build_omnibus

      expect(builder).to have_received(:new)
        .with(task.branch, 'b81056529d1f', task.metadata)
      expect(builder).to have_received(:execute)
    end
  end

  describe '#build_cng' do
    it 'executes CNG builder' do
      stub_passing_build('79def06ea756')
      builder = stub_const('ReleaseTools::AutoDeploy::Builder::CNGImage', spy)

      task.build_cng

      expect(builder).to have_received(:new)
        .with(task.branch, '79def06ea756', task.metadata)
      expect(builder).to have_received(:execute)
    end
  end

  describe '#tag_coordinator' do
    it 'executes Coordinator tagger' do
      tagger = stub_const('ReleaseTools::AutoDeploy::Tagger::Coordinator', spy)

      task.tag_coordinator

      expect(tagger).to have_received(:new)
      expect(tagger).to have_received(:tag!)
    end
  end

  describe '#sentry_release' do
    it 'tracks a Sentry release' do
      stub_passing_build('cdec0111a65a')
      tracker = stub_const('ReleaseTools::Deployments::SentryTracker', spy)

      task.sentry_release

      expect(tracker).to have_received(:new)
      expect(tracker).to have_received(:release).with('cdec0111a65a')
    end
  end

  describe '#upload_metadata' do
    it 'uploads release metadata' do
      uploader = stub_const('ReleaseTools::ReleaseMetadataUploader', spy)

      expect(ReleaseTools::AutoDeploy::Tag).to receive(:current)
        .and_return('1.2.3')

      task.upload_metadata

      expect(uploader).to have_received(:new)
      expect(uploader).to have_received(:upload)
        .with('1.2.3', task.metadata)
    end
  end
end
